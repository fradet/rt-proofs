(* Time is defined as a natural number. *)

Definition duration := nat.
Definition instant  := nat.
